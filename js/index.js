$(function () {
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();
  $(".carousel").carousel({
    interval: 6000,
  });
  $("#enviarModal").on("show.bs.modal", function (e) {
    console.log("Se está mostrando el modal");
    $("#enviarBtn").removeClass("btn-success");
    $("#enviarBtn").addClass("btn-secondary");
    $("#enviarBtn").prop("disabled", true);
  });
  $("#enviarModal").on("shown.bs.modal", function (e) {
    console.log("Se mostró el modal");
  });
  $("#enviarModal").on("hide.bs.modal", function (e) {
    console.log("Se está cerrando el modal");
  });
  $("#enviarModal").on("hidden.bs.modal", function (e) {
    console.log("Se cerró el modal");
    $("#enviarBtn").removeClass("btn-secondary");
    $("#enviarBtn").addClass("btn-success");
    $("#enviarBtn").prop("disabled", false);
  });
});
